module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/~cs62160139/learn_bootstrap/'
    : '/'
}
